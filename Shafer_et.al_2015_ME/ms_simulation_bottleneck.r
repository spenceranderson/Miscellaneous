N0=10000
nreps=12500
sample_size=40
nbloci=50000
Ts_bottle=runif(nreps,min=0.001,max=0.2)
#Ts_recover=runif(nreps,min=0.001,max=0.1)
Ts_bottle_size=runif(nreps,min=0.001,max=0.1)
#Ts_recover_size=runif(nreps,min=0.01,max=1)
MAF=4

#simulated
Model_bottle_mean_sumstats=matrix(0,nreps,10)
Model_bottle_sd_sumstats=matrix(0,nreps,10) 
for (irep in 1:nreps) {
	threshold=nbloci
tbs_file=0
final_SNPs=NULL
while (tbs_file < threshold) {
  trial=rpois(nbloci,0.09512)
  SNPs=subset(trial,trial>0 & trial<5)
  final_SNPs=c(final_SNPs,SNPs)
  tbs_file=tbs_file+length(SNPs)  
}
final_SNPs=head(final_SNPs,nbloci)
write.table(final_SNPs,file="segsites",row.names=F, col.names=F)

	if (irep%%100==0) print(irep)
	system(paste("./msnsam ",sample_size," ",nbloci," -s tbs -F ",MAF," -eN ",Ts_bottle[irep]," ",Ts_bottle_size[irep]," <segsites > ms.out1",sep=""))
	system(paste("./msABC ",sample_size," ",nbloci," --obs ms.out1 > ms.obs.mean1",sep=""))
	table=read.table("ms.obs.mean1",header=TRUE)
	Model_bottle_mean_sumstats[irep,]=colMeans(table)
	Model_bottle_sd_sumstats[irep,]=apply(table,2,sd)
}

write.table(cbind(Ts_bottle,Ts_bottle_size,Model_bottle_mean_sumstats),"Model_bottle_mean_sumstats", col.names=c("Ts_b","Ts_b_size",names(table)),row.names=FALSE,quote=F)
write.table(cbind(Ts_bottle,Ts_bottle_size,Model_bottle_sd_sumstats),"Model_bottle_sd_sumstats", col.names=c("Ts_b","Ts_b_size",names(table)),row.names=FALSE,quote=F)

#plot(Ts_prior,Model1_mean_sumstats[,1])


#TRUE_Model
s=(rpois(nbloci,0.3))+1
write.table(s,file="segsites",row.names=F, col.names=F)
system(paste("./msnsam ",sample_size," ",nbloci," -s tbs -F ",MAF," -eN 0.12 0.01 <segsites >Model_bottle_obs_stats",sep=""))
system(paste("./msABC ",sample_size," ",nbloci," --obs Model_bottle_obs_stats > ms.bottle.obs.mean",sep=""))
table2=read.table("ms.bottle.obs.mean", header=T)
Model_bottle_obs_sumstats=colMeans(table2)
Model_bottle_sd_obs_sumstats=apply(table2,2,sd)

write.table(Model_bottle_obs_sumstats,"Model_bottle_obs_sumstats",quote=F)
write.table(Model_bottle_sd_obs_sumstats,"Model_bottle_sd_obs_sumstats",quote=F)



