args <- commandArgs(trailingOnly = TRUE)
outfilename=args[1]
log10_theta_values=-2
theta=10^(log10_theta_values)
#theta=runif(nrepsABC,min=0.001, max=0.1)
msnreps=4854
threshold=4854
sample_size=60
#MAF=0.05*sample_size
nrepsABC=1000
m1_prior=runif(nrepsABC,min=1.1969 ,max=19.9920)
m2_prior=runif(nrepsABC,min=10.7573,max=19.9960)
Ts_prior=runif(nrepsABC,min=0.2709,max=1.9767)

for (irep in 1:msnreps) {
 	 	
 	 	sumstats_table=matrix(0,nrepsABC,10)
 	
 	for (irepABC in 1:nrepsABC) {
 
 		S1_class=0
 		final_table=NULL
 		while (S1_class<threshold) {
 
 	
 			
 			system(paste("./msABC ",sample_size," ",msnreps," -t ",theta," -r 0 75 -I 2 20 40 -m 1 2 ",m1_prior[irepABC]," -m 2 1 ",m2_prior[irepABC]," -ej ",Ts_prior[irepABC]," 2 1 --verbose > ms.out",sep=""))
 			table <- read.table("ms.out", header=T)
 			indices_class1=which(table$s_fst!="-NaN"&table$s_fst!="NaN"&table$s_segs==1)	
 			S1_class=S1_class+length(indices_class1)
 			final_table=rbind(final_table,table[indices_class1,])		
 		}
 		summary_stats=c(colMeans(final_table[1:threshold,c(1,4,5,6,7,8,9,12,22,29),drop=F]))
 		sumstats_table[irepABC,]=t(summary_stats)
 		theta_value=rep(theta,threshold)
 		write.table(sumstats_table,file=paste(outfilename,"out_running",sep=""),row.names=F,quote=F,col.names=names(summary_stats),sep="\t",append=F)	 	
	}
 	write.table(sumstats_table,file="mu_modelX_Xloci",row.names=FALSE,quote=F,col.names=names(summary_stats),sep="\t")
 quit()
 }
c
 

